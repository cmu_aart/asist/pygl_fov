import pygame
from OpenGL.GL import *
from ctypes import *

pygame.init()
screen = pygame.display.set_mode((800,600), pygame.OPENGL|pygame.DOUBLEBUF, 20)
glViewport(0,0,800,600)
glClearColor(0.0,0.5,0.5,1.0)
#glEnableClientState(GL_VERTEX_ARRAY)

vertices = [0.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 0.0, 0.0]
colors =   [1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0,
            1.0, 0.0, 1.0]

vertex_vbo = glGenBuffers(1)
color_vbo = glGenBuffers(1)

glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo)
glBufferData(GL_ARRAY_BUFFER, len(vertices)*4, (c_float*len(vertices))(*vertices), GL_STATIC_DRAW)

glBindBuffer(GL_ARRAY_BUFFER, color_vbo)
glBufferData(GL_ARRAY_BUFFER, len(colors)*4, (c_float*len(colors))(*colors), GL_STATIC_DRAW)

running = True
while running:

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False

	glClear(GL_COLOR_BUFFER_BIT)

	glEnableClientState(GL_VERTEX_ARRAY)
	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo)
	glVertexPointer(3, GL_FLOAT, 0, None)

	glEnableClientState(GL_COLOR_ARRAY)
	glBindBuffer(GL_ARRAY_BUFFER, color_vbo)
	glColorPointer(3, GL_FLOAT, 0, None)

	glDrawArrays(GL_QUADS, 0, 4)

	glDisableClientState(GL_COLOR_ARRAY)
	glDisableClientState(GL_VERTEX_ARRAY)

	pygame.display.flip()

