"""
block.py

This file defines a lightweight class encapsulating attributes of interest of
Minecraft blocks.
"""

import logging

import MinecraftElements


class BlockIdRegistry:
	"""
	A singleton class for generating and maintaining unique identifiers for
	each block.
	"""

	__instance = None

	@staticmethod
	def getInstance():
		"""
		Get the instance of the BlockIdRegistry
		"""

		if BlockIdRegistry.__instance is None:
			BlockIdRegistry()

		return BlockIdRegistry.__instance


	def __init__(self):
		"""
		Create the BlockIdRegistry
		"""

		self.nextId = 0

		if BlockIdRegistry.__instance is not None:
			raise Exception("Attempting to create non-singleton instance of BlockIdRegistry.")
		else:
			BlockIdRegistry.__instance = self


	def getId(self):
		"""
		Return the next unique block id
		"""

		# Grab the next Id, and increment
		_id = self.nextId
		self.nextId += 1

		return _id


	def idCount(self):
		"""
		Return the number of id's that have been registered
		"""

		return self.nextId


class Block:
	"""
	A lightweight representation of a block in Minecraft.  This class contains,
	at a minimum, the needed components for FoV membership calculation (ID and
	location).  Subclassing or decorating allows for inclusion of additional 
	attributed desired by a client (e.g., material, color, etc.).

	Defined attributes include:

		id:       a unique identifier used for membership calculation, which is
		          generated by a block feeder during iteration.
		location: the (x,y,z) location of the block in Minecraft
	"""

	def fromDict(cls, data):
		"""
		Create a Block instance using a provided dictionary.  Attributes (keys)
		of the block include

		location - triple containing the (x,y,z) location of the block
		type     - string type of the 

		Args:
			data - a dictionary containing information about the block.
		"""

		pass


	def __init__(self, location, block_type, facing=None, 
		               attached=None, powered=None, half=None, shape=None,
		               east=False, west=False, north=False, south=False,
		               hinge=None, in_wall=False, isopen=False,
		               layers=0, orientation=(0.0,0.0,0.0),
		               playername=None, marker_type=None, victim_id=None):
		"""
		Create a new block.  The id of the block will automatically be created
		using the BlockIdRegistry instance, to ensure the block is unique.

		Certain block attributes (e.g., open) can be modified.  When a block
		attribute is modified, it will call the blockModified method of all
		registered listeners, sending itself as an argument.

		Args:
			location   - the (x,y,z) position of the block.  The argument-type
			             needs to be able to be cast into a 3-tuple 
			             (e.g., list, tuple, ndarray)
			block_type - optional enum value of MinecraftElements.Block
			facing     - optional 
			attached
			powered    - is the block powered
			half       -
			shape      -
			east       -
			west       -
			north      -
			south      -
			hinge      -
			in_wall    -
			isopen     -
			layers     -
		"""

		self.logger = logging.getLogger(__name__)

		self._id = BlockIdRegistry.getInstance().getId()
		self._location = tuple(location)
		self._block_type = block_type
		self._facing = facing
		self._attached = attached
		self._powered = powered
		self._half = half
		self._hinge = hinge
		self._shape = shape
		self._east = east
		self._west = west
		self._north = north
		self._south = south
		self._in_wall = in_wall
		self._open = isopen
		self._layers = layers
		self._orientation = orientation
		self._playername = playername
		self._marker_type = marker_type
		self._victim_id = victim_id


		# Observers will be informed when a block is changed
		self.__observers = set()


	def __str__(self):
		"""
		String representation of the Block
		"""

		return "[Block %d <%s> @ (%d,%d,%d)]" % (self.id, self.block_type.name, self.location[0], self.location[1], self.location[2])


	def register(self, observer):
		"""
		Add an observer to the block
		"""

		if observer in self.__observers:
			self.logger.debug("%s:  Attemting to add already registered observer: %s", self, observer)

		self.__observers.add(observer)


	def deregister(self, observer):
		"""
		Remove a observer to the block
		"""

		if not observer in self.__observers:
			self.logger.debug("%s:  Attemting to remove non-registered observer: %s", self, observer)

		self.__observers.remove(observer)


	def __notify(self):
		"""
		Inform observers that the Block has changed.
		"""

		for observer in self.__observers:
			observer.onBlockModified(self)


	@property
	def id(self):
		return self._id

	@id.setter
	def id(self, block_id):
		self._id = block_id

	@property
	def location(self):
		return self._location
	
	@location.setter
	def location(self, location):
		self._location = location
		self.__notify()

	@property
	def open(self):
		return self._open
	
	@open.setter
	def open(self, isopen):
		self._open = isopen
		self.__notify()

	@property
	def layers(self):
		return self._layers
	
	@layers.setter
	def layers(self, layers):
		self._layers = layers
		self.__notify()

	@property
	def block_type(self):
		return self._block_type

	@block_type.setter
	def block_type(self, block_type):
		self._block_type = block_type
		self.__notify()

	@property
	def facing(self):
		return self._facing

	@facing.setter
	def facing(self, facing):
		self._facing = facing
		self.__notify()

	@property
	def attached(self):
		return self._attached
	
	@attached.setter
	def attached(self, attached):
		self._attached = attached
		self.__notify()

	@property
	def powered(self):
		return self._powered
	
	@powered.setter
	def powered(self, powered):
		self._powered = powered
		self.__notify()

	@property
	def half(self):
		return self._half
	
	@half.setter
	def half(self, half):
		self._half = half
		self.__notify()

	@property
	def hinge(self):
		return self._hinge
	
	@hinge.setter
	def hinge(self, hinge):
		self._hinge = hinge
		self.__notify()

	@property
	def shape(self):
		return self._shape
	
	@shape.setter
	def shape(self, shape):
		self._shape = shape
		self.__notify()

	@property
	def east(self):
		return self._east
	
	@east.setter
	def east(self, east):
		self._east = east
		self.__notify()

	@property
	def west(self):
		return self._west
	
	@west.setter
	def west(self, west):
		self._west = west
		self.__notify()

	@property
	def north(self):
		return self._north
	
	@north.setter
	def north(self, north):
		self._north = north
		self.__notify()

	@property
	def south(self):
		return self._south
	
	@south.setter
	def south(self, south):
		self._south = south
		self.__notify()

	@property
	def in_wall(self):
		return self._in_wall
	
	@in_wall.setter
	def in_wall(self, in_wall):
		self._in_wall = in_wall
		self.__notify()


	@property
	def orientation(self):
		return self._orientation

	@orientation.setter
	def orientation(self, orientation):
		self._orientation = orientation
		self.__notify()
	
	@property
	def playername(self):
		return self._playername

	@playername.setter
	def playername(self, name):
		self._playername = name
		self.__notify()


	@property
	def marker_type(self):
		return self._marker_type

	@marker_type.setter
	def marker_type(self, marker_type):
		self._marker_type = marker_type
		self.__notify()

	@property
	def victim_id(self):
		return self._victim_id

	@victim_id.setter
	def victim_id(self, _id):
		self._victim_id = _id
		self.__notify()

