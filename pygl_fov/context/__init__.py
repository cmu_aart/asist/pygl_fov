"""
This folder contains available contexts for OpenGL to render to.

Available Context Backends:

* GLFW
* GLUT
"""