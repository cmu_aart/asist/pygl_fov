B
    m��a�"  �               @   sX   d Z ddlmZmZ yddlmZ W n   ddlmZ Y nX ddlZG dd� d�Z	dS )a�  
perspective.py

The Perspective class establishes and maintains all components associated with
the perspective / camera in OpenGL.  In particular, the class stores the
field of view and size of the window to render to.

The main purpose of keeping the perspective classes separate from the FOV class
is to allow for creation of the OpenGL context based on available / installed
libraries.  At the moment, GLFW is the preferred context, while GLUT is used as
a backup context.
�    )�GL�GLU�   )�OpenGLContextNc               @   s�   e Zd ZdZG dd� d�Zedd�e_edd�e_edd�e_ddejdd	d
fdd�Zdd� Z	dd� Z
dd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd � Zd!d"� Zd#S )$�Perspectiveal  
	The perspective class is responsible for maintaining components related to
	the perspective model in OpenGL, and inform the FOV when any of these
	components change.  Specifically, instances of the class need to keep 
	track of the position and orientation of the perspective, the vertical
	angle of the field of view, and the size of the window (if possible).
	c               @   s   e Zd ZdZdd� ZdS )zPerspective.PlayerStatez�
		The PlayerState class is a simple wrapper encapsulating the effect of
		the player's current movement state (e.g., running, walking, etc.) has
		on the perspective of the FoV
		c             C   s   || _ || _dS )a  
			Create a new PlayerState with the associated perspective parameters

			Args:
				viewHeight - the vertical offset of the perspective from the
				             player's position
				fovy       - the y-axis field of view (in degrees) of the
				             perspective
			N)�
viewHeight�fovy)�selfr   r   � r
   �2c:\research\asist\pygl_fov\pygl_fov\perspective.py�__init__)   s    z Perspective.PlayerState.__init__N)�__name__�
__module__�__qualname__�__doc__r   r
   r
   r
   r   �PlayerState"   s   r   g�������?�F   g      �?)r   r   r   )iX  i�  g�������?g      Y@c             C   sZ   || _ || _|| _|| _|| _|| _t| jd �t| jd � | _t| j�| _	t
� | _dS )a�  
		Create the perspective with the provided parameters.

		Args:
		    position    - the location (x,y,z) of the perspective
		    orientation - the orientation (pitch, yaw, roll) of the perspective
		    playerState - the player's motion state, an instance of PlayerState
		    window_size - the size of the window to render (wicth, height)
		    zNear       - distance to the near clipping plane
		    zFar        - distance to the far clipping plane
		r   r   N)�position�orientation�playerState�window_size�zNear�zFar�float�aspectr   �context�set�	observers)r	   r   r   r   r   r   r   r
   r
   r   r   I   s    zPerspective.__init__c             C   s   | j �|� dS )z�
		Add an observer to the set of observers of this object.

		Args:
			observer - object to be notified when the feeder contents change.
		N)r   �add)r	   �observerr
   r
   r   �registerk   s    zPerspective.registerc             C   s   || j kr| j �|� dS )z�
		Remove the observer from the set of observers of this object.

		Args:
			observer - object in the observers registry to be removed.
		N)r   �remove)r	   r   r
   r
   r   �
deregisterv   s    
zPerspective.deregisterc             C   s   x| j D ]}|��  qW dS )z8
		Inform observers that the perspective has changed.
		N)r   �updatePerspective)r	   r   r
   r
   r   �__notify�   s    zPerspective.__notifyc             C   s�   | j \}}}| j\}}}|| jj7 }|d7 }t�tj� t��  t�	| jj
| j| j| j� t�tj� t��  t�|ddd� t�|d ddd� t�|ddd� t�| | | � dS )zo
		Set the OpenGL projection and model matrices to reflect the position
		and orientation of the perspective
		g        g      �?�   N)r   r   r   r   r   �glMatrixMode�GL_PROJECTION�glLoadIdentityr   ZgluPerspectiver   r   r   r   �GL_MODELVIEW�	glRotatef�glTranslatef)r	   �x�y�z�pitch�yaw�rollr
   r
   r   �setup�   s    zPerspective.setupc             C   s   || _ || _dS )z�
		Set the position and orientation of the perspective

		Args:
		    position    - (x,y,z) of the updated perspective 
		    orientation - (pitch,yaw,roll) of the updated perpective
		N)r   r   )r	   r   r   r
   r
   r   �set_pose�   s    	zPerspective.set_posec             C   s   | � || j� dS )zj
		Set the position of the perspective

		Args:
		    position    - (x,y,z) of the updated perspective 
		N)r3   r   )r	   r   r
   r
   r   �set_position�   s    zPerspective.set_positionc             C   s   | � | j|� dS )zt
		Set the orientation of the perspective

		Args:
		    orientation - (pitch,yaw,roll) of the updated perpective
		N)r3   r   )r	   r   r
   r
   r   �set_orientation�   s    zPerspective.set_orientationc             C   sx   t �t jd� t �dd| jd | jd t jt j�}t�|tj	�}| jd | jd df|_
t�|dd�}tj|dd�}|S )z�
		Extract the image generated by the OpenGL rendering

		Returns:
			A 3D numpy array containing the RGB values of each pixel
		r   r   �   )�axis)r   �glPixelStorei�GL_PACK_ALIGNMENT�glReadPixelsr   �GL_RGB�GL_UNSIGNED_BYTE�np�
frombuffer�ubyte�shape�swapaxes�flip)r	   �dataZrgbArrayr
   r
   r   �getImage�   s    	zPerspective.getImagec             C   sn   | j d tj d }| j d d tj d }t�|�t�|� }t�|�t�|� }t�|�}tj|||f S )zG
		Return a normalized vector representing the direction of the view
		r   g     �f@r   )r   r=   �pi�cos�sin�array)r	   r/   r0   r,   r.   r-   r
   r
   r   �
getViewRay�   s    
zPerspective.getViewRayc             C   sr   | j d tj d }| j d d tj d }t�|� t�|� }t�|� t�|� }t�|�}tj|||f S )z%
		Return the up and right vectors
		r   g     �f@r   )r   r=   rE   rF   rG   rH   )r	   r/   r0   r,   r.   r-   r
   r
   r   �getUp�   s    
zPerspective.getUpc             C   s�  | � � }dt�| jjtj d � | j }|| j }dt�| jjtj d � | j }|| j }| �	� }t�
||�}t�| j�}||| j  }	||| j  }
|	|| d  || d  }|	|| d  || d  }|	|| d  || d  }|	|| d  || d  }|
|| d  || d  }|
|| d  || d  }|
|| d  || d  }|
|| d  || d  }|
|| d  | }|
|| d  | }|
|| d  | }|
|| d  | }|
|f}|	| f}dS )z
		Compute the frustrum
		g       @g     �v@�   N)rI   r=   �tanr   r   rE   r   r   r   rJ   �crossrH   r   )r	   ZviewRayZHnearZWnearZHfarZWfar�up�right�p�fcZncZftlZftrZfblZfbrZntlZntrZnblZnbrZaRightZaLeftZaTopZaBottomZ	nearPlaneZfarPlaner
   r
   r   �computeFrustrum	  s0    "
"
zPerspective.computeFrustrumN)r   r   r   r   r   ZWALKINGZ	SPRINTINGZSNEAKINGr   r    r"   Z_Perspective__notifyr2   r3   r4   r5   rD   rI   rJ   rR   r
   r
   r
   r   r      s(   "	"r   )
r   �OpenGLr   r   Zcontext.glfwr   Zcontext.glut�numpyr=   r   r
   r
   r
   r   �<module>   s   